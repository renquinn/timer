# timer.go

A *very* simple timer library that saves but a few keystrokes when timing parts
of go code.

### Installation

go get gitlab.com/renquinn/timer

### Usage

```
import "gitlab.com/renquinn/timer"
```

Init timer:

```
t := timer.New()
```

Start timer:

```
t.A()
```

Stop timer, printing results:

```
t.B()
```

Outputs a duration:

```
Took: 415.619223ms
```
