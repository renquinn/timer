package timer

import (
	"fmt"
	"time"
)

type Timer struct {
	Start time.Time
}

func (t *Timer) A() {
	t.Start = time.Now()
}

func (t *Timer) B() {
	fmt.Println("Took:", time.Now().Sub(t.Start))
}

func New() *Timer {
	return new(Timer)
}
