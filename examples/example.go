package main

import (
	"gitlab.com/renquinn/timer"
)

func main() {
	t := timer.New()
	t.A()
	defer t.B()
	for i := 0; i < 1000000000; i++ {
		i += 1
	}
}
